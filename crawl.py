import urllib2
import json
import parser

term = "Obama"

resource = urllib2.urlopen("http://search.twitter.com/search.json?lang=en&q=" + term + "&rpp=100")
data = json.load(resource)

arrayTotal = len(data["results"])
for i in range(0,arrayTotal):

	user = data["results"][i]["from_user"]
	tweet = data["results"][i]["text"]
	rank = parser.rank(tweet)

	if rank != 0:

		print "User: @" + user
		print "Tweet: " + tweet
		print "Sentiment: " + str(rank)
		print
		print
