import pool
from string import ascii_letters, digits
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

def rank(tweet):

	mark = 0

	positive = pool.positive
	negative = pool.negative
	stoplist = pool.stop
	tweet = tweet.lower()
	tweet = "".join([ch for ch in tweet if ch in (ascii_letters + digits + " ")]) #remove punctuation

	tweet = tweet.split()
	wordArray = [x for x in tweet if x not in set(stoplist)] 

	i = 0
	while i < len(wordArray) - 1:
		i += 1
		wordloop = wordArray[i]


		for positiveloop in positive:
			fuzzeh = fuzz.ratio(wordloop, positiveloop)
			calculate = 0


			if fuzzeh > 80:
				value = positive[positiveloop]
				calculate = 1


			if calculate is 1:
				for word in wordArray:
						if word in positive:
							value = positive[word]
							


				if wordArray[i] is not -1:
					if wordArray[i-1] == "not":
						mark -= value
					else:
						mark += value


		for negativeloop in negative:
			fuzzeh = fuzz.ratio(wordloop, negativeloop)
			calculate = 0

			if fuzzeh > 80:
				value = negative[negativeloop]
				calculate = 1


			if calculate is 1:
				for word in wordArray:
						if word in negative:
							value = negative[word]
							

				if wordArray[i] is not -1:
					if wordArray[i-1] == "not":
						mark += value
					else:
						mark -= value


	return mark;